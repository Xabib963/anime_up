import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../data/datasources/GenerServices.dart';

part 'gener_event.dart';
part 'gener_state.dart';

class GenerBloc extends Bloc<GenerEvent, GenerState> {
  final GenerServices generServices;
  String generSelected = '';
  GenerBloc({required this.generServices}) : super(GenerInitial()) {
    List<String> gener = [];
    on<GetGener>((event, emit) async {
      emit(GenerInitial());
      await Future.delayed(Duration(seconds: 2));
      try {
        gener = await generServices.getAnimeGenere();
        emit(GenerLoaded(gener: gener));
      } catch (e) {
        emit(ErrorStateG(error: e.toString()));
      }
    });
    on<SelectGener>((event, emit) {
      emit(GenerLoaded(gener: gener, selectedid: event.id));
    });
  }
}
