part of 'search_bloc.dart';

abstract class SearchState extends Equatable {
  const SearchState();

  @override
  List<Object> get props => [];
}

class SearchInitial extends SearchState {}

class SearchListLoaded extends SearchState {
  final List<Animes> dataList;

  const SearchListLoaded(this.dataList);
  @override
  List<Object> get props => [dataList];
}

class ErrorState extends SearchState {
  final String error;

  const ErrorState(this.error);
  @override
  List<Object> get props => [error];
}
