part of 'anime_by_genre_bloc.dart';

abstract class AnimeByGenreEvent extends Equatable {
  const AnimeByGenreEvent();

  @override
  List<Object> get props => [];
}

class GetAnimeByGener extends AnimeByGenreEvent {
  final String genre;

  GetAnimeByGener({required this.genre});
  @override
  List<Object> get props => [genre];
}
