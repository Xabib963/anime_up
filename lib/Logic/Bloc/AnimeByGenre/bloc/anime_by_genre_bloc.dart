import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../data/models/AnimesModel.dart';
import '/jsons/Movies.dart';

part 'anime_by_genre_event.dart';
part 'anime_by_genre_state.dart';

class AnimeByGenreBloc extends Bloc<AnimeByGenreEvent, AnimeByGenreState> {
  List<Animes> animes =
      movies['data'].map<Animes>((ele) => Animes.fromJson(ele)).toList();

  AnimeByGenreBloc() : super(AnimeByGenreInitial()) {
    on<GetAnimeByGener>((event, emit) async {
      emit(AnimeByGenreInitial());
      List<Animes> animeByGenre = [];
      for (Animes anime in animes) {
        if (anime.genres!.contains(event.genre)) {
          animeByGenre.add(anime);
        }
      }
      emit(AnimeByGenreInitial());
      await Future.delayed(Duration(seconds: 2));
      emit(AnimeByGenreLoaded(anime: animeByGenre));
    });
  }
}
