// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:read_more_text/read_more_text.dart';

import 'package:ani_me/core/AppRouter.dart';

import '../../core/Theme.dart';
import '../../core/settings.dart';
import '../../data/models/AnimesModel.dart';
import 'widget/ReadMoreCustom.dart';
import 'widget/SpanCustomText.dart';

class AnimeDetiels extends StatelessWidget {
  AnimeDetiels({super.key});
  static const routeName = '/detiels';

  @override
  Widget build(BuildContext context) {
    Animes args = ModalRoute.of(context)!.settings.arguments as Animes;
    return Scaffold(
      body: SafeArea(
          child: SizedBox(
        width: media(context).width,
        height: media(context).height,
        child: Stack(children: [
          Positioned.fill(
              child: Image.network(
            opacity: const AlwaysStoppedAnimation(0.5),
            args.image!,
            errorBuilder: (context, error, stackTrace) =>
                const Icon(Icons.error, color: Colors.red),
            fit: BoxFit.fill,
          )),
          Positioned.fill(
              child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                  child: Container(
                    color: Colors.black.withOpacity(0.5),
                  ))),
          Padding(
            padding: const EdgeInsets.only(
                left: 20.0, right: 20, top: 30, bottom: 10),
            child: SizedBox(
              height: media(context).height,
              child: Column(
                children: [
                  Expanded(
                    flex: 3,
                    child: SizedBox(
                      width: media(context).width * 0.85,
                      child: Card(
                        color: Colors.transparent,
                        elevation: 20,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18)),
                        clipBehavior: Clip.hardEdge,
                        margin: const EdgeInsets.all(0),
                        child: Stack(clipBehavior: Clip.hardEdge, children: [
                          Positioned.fill(
                              child: Image.network(
                            errorBuilder: (context, error, stackTrace) =>
                                const Icon(Icons.error, color: Colors.red),
                            fit: BoxFit.fill,
                            args.image!,
                          )),
                          Positioned(
                            top: 7,
                            left: 7,
                            child: CircleAvatar(
                              radius: 15,
                              backgroundColor:
                                  Colors.grey[500]!.withOpacity(0.7),
                              child: IconButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  icon: const Icon(
                                    Icons.arrow_back_ios_new_rounded,
                                    color: Colors.white,
                                    size: 10,
                                    fill: 0.7,
                                    opticalSize: 0.8,
                                  )),
                            ),
                          ),
                          Positioned(
                            top: 7,
                            right: 7,
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 3),
                              decoration: BoxDecoration(
                                  // gradient: LinearGradient(
                                  //     colors: [
                                  //       Color.fromRGBO(241, 174, 0, 1),
                                  //       Color.fromRGBO(255, 220, 197, 1),
                                  //       Color.fromRGBO(150, 19, 173, 1),
                                  //     ],
                                  //     begin: Alignment.bottomCenter,
                                  //     end: Alignment.topLeft),
                                  color: Colors.grey[500]!.withOpacity(0.7),
                                  borderRadius: BorderRadius.circular(10)),
                              width: media(context).width * 0.15,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  IconButton(
                                      onPressed: () {},
                                      icon: const Icon(
                                        Icons.favorite,
                                        color: Colors.red,
                                        size: 25,
                                        fill: 0.7,
                                        opticalSize: 0.8,
                                      )),
                                  Text(
                                    '5M',
                                    style: textStyle(
                                        size: 13,
                                        color: Colors.white,
                                        weight: FontWeight.normal),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Positioned(
                            top: media(context).width * 0.25,
                            right: 7,
                            child: Container(
                              width: media(context).width * 0.15,
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 3),
                              decoration: BoxDecoration(
                                  color: Colors.grey[500]!.withOpacity(0.7),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    '  EP  ',
                                    style: textStyle(
                                        size: 18,
                                        color: Colors.white,
                                        weight: FontWeight.bold),
                                  ),
                                  Text(
                                    args.episodes.toString(),
                                    style: textStyle(
                                        size: 13,
                                        color: Colors.white,
                                        weight: FontWeight.normal),
                                  )
                                ],
                              ),
                            ),
                          )
                        ]),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              args.title!,
                              style: textStyle(
                                  color: Colors.white,
                                  size: 18,
                                  weight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            width: media(context).width,
                            height: media(context).height * 0.06,
                            child: ListView.builder(
                                itemCount: args.genres!.length,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) =>
                                    HorizentalListItems(
                                        args: args.genres![index])),
                          ),
                          SizedBox(
                            width: media(context).width * 0.82,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SpanCustomText(
                                    title: 'Status',
                                    body: args.status!,
                                    sizeT: 13,
                                    sizeB: 16),
                                RichText(
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'Dietels :',
                                      style: textStyle(
                                          color: Colors.white,
                                          size: 16,
                                          weight: FontWeight.bold),
                                    ),
                                  ]),
                                ),
                                ReadMoreCustom(args: args.synopsis!),
                                SizedBox(
                                  width: media(context).width,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      SpanCustomText(
                                          title: 'Ranking',
                                          body: args.ranking.toString(),
                                          sizeT: 13,
                                          sizeB: 16),
                                      SpanCustomText(
                                          title: 'Type',
                                          body: args.type!,
                                          sizeT: 13,
                                          sizeB: 16),
                                    ],
                                  ),
                                ),
                                RichText(
                                  text: TextSpan(children: [
                                    TextSpan(
                                      text: 'Alternative Titles :',
                                      style: textStyle(
                                          color: Colors.white,
                                          size: 16,
                                          weight: FontWeight.bold),
                                    ),
                                  ]),
                                ),
                                SizedBox(
                                  width: media(context).width,
                                  height: media(context).height * 0.06,
                                  child: ListView.builder(
                                    itemCount: args.alternativeTitles!.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) =>
                                        HorizentalListItems(
                                      args: args.alternativeTitles![index],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ]),
      )),
    );
  }
}

class HorizentalListItems extends StatelessWidget {
  const HorizentalListItems({
    Key? key,
    required this.args,
  }) : super(key: key);

  final String args;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.amber[600]!.withOpacity(0.3),
          borderRadius: BorderRadius.circular(15)),
      margin: const EdgeInsets.all(5),
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: Text(
        args,
        style:
            textStyle(size: 15, color: Colors.white, weight: FontWeight.bold),
      ),
    );
  }
}
