import 'package:flutter/material.dart';
import 'package:read_more_text/read_more_text.dart';

import '../../../core/Theme.dart';

class ReadMoreCustom extends StatelessWidget {
  const ReadMoreCustom({
    super.key,
    required this.args,
  });

  final String args;

  @override
  Widget build(BuildContext context) {
    return ReadMoreText(
      readMoreIconColor: Colors.amber[600]!,
      readMoreTextStyle: textStyle(
          color: Colors.amber[600]!, size: 14, weight: FontWeight.bold),
      style:
          textStyle(color: Colors.white, size: 16, weight: FontWeight.normal),
      args,
      numLines: 3,
      readMoreText: 'Read more',
      readLessText: 'Read less',
    );
  }
}
