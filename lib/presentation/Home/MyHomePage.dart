import 'dart:ui';

import 'package:ani_me/Logic/Bloc/AnimeByGenre/bloc/anime_by_genre_bloc.dart';
import 'package:ani_me/core/settings.dart';
import 'package:ani_me/data/models/AnimesModel.dart';
import 'package:ani_me/presentation/SearchPage/SearchPage.dart';
import 'package:ani_me/presentation/loadWidget/SkeletonGrid.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/Theme.dart';
import '../../Logic/Bloc/AnimeBloc/anime_bloc.dart';
import '../../Logic/Bloc/GenerBloc/gener_bloc.dart';

import '../../core/loadingEffict.dart';
import '../AnimeDieatels/AnimeDiatels.dart';
import 'widget/CarouselCard.dart';
import 'widget/CarouselCardDesighn2.dart';
import 'widget/CustomInput.dart';
import 'widget/CustomSearchDelegate.dart';
import '../loadWidget/GenerSkeleton.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);
  static const routeName = '/';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          Positioned(
              bottom: 0,
              right: 0,
              child: Image.asset(
                opacity: const AlwaysStoppedAnimation(0.9),
                'assets/pngwing.com.png',
                fit: BoxFit.fitWidth,
                width: media(context).width * 0.6,
              )),
          Positioned.fill(
              child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                  child: SizedBox())),
          Padding(
            padding: const EdgeInsets.only(top: 00.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CustomInput(),
                  ),
                  Row(
                    children: [
                      Icon(
                        size: media(context).width * 0.08,
                        Icons.movie_creation_outlined,
                        color: Colors.amber,
                      ),
                      Text(
                        '  Top Movies :',
                        style: textStyle(
                            size: media(context).width * 0.05,
                            color: Colors.grey[600]!,
                            weight: FontWeight.bold),
                      )
                    ],
                  ),
                  BlocBuilder<AnimeBloc, AnimeState>(
                      bloc: context.read<AnimeBloc>()
                        ..add(GetMovieEvent(genre: 'Action')),
                      builder: (context, state) {
                        if (state is AnimeInitial) {
                          return SizedBox(
                            width: media(context).width,
                            height: media(context).height * 0.3,
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Skeleton.rectangular(
                                      width: media(context).width * 0.6,
                                      height: media(context).height * 0.2),
                                  Skeleton.rectangular(
                                      width: media(context).width * 0.3,
                                      height: media(context).height * 0.2),
                                ]),
                          );
                        } else if (state is AnimeLoaded) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 20.0),
                            child: CarouselSlider(
                                options: CarouselOptions(
                                  aspectRatio: 16 / 7.8,
                                  initialPage: 1,
                                  enableInfiniteScroll: false,
                                  autoPlay: false,
                                  viewportFraction: 0.8,
                                  scrollDirection: Axis.horizontal,
                                ),
                                items: state.animes
                                    .map((e) => CarouselCardDesighn2(anime: e))
                                    .toList()),
                          );
                        } else if (state is ErrorState) {
                          //  Padding(
                          //                 padding: const EdgeInsets.symmetric(
                          //                     horizontal: 3.0),
                          //                 child: CarouselCard(
                          //                   anime: e,
                          //                   txt: e.title!,
                          //                   image: e.image!,
                          //                 ),
                          //               )
                          print(state.error.toString());
                          return SizedBox();
                        } else {
                          return SizedBox();
                        }
                      }),
                  BlocBuilder<GenerBloc, GenerState>(
                    bloc: context.read<GenerBloc>()..add(GetGener()),
                    builder: (context, state) {
                      if (state is GenerInitial) {
                        return const Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GenerSkeleton(),
                        );
                      }
                      if (state is GenerLoaded) {
                        return SizedBox(
                          height: media(context).height * 0.08,
                          width: media(context).width * 0.9,
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: state.gener.length,
                            itemBuilder: (context, index) => Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              child: InkWell(
                                splashColor: Colors.grey.withOpacity(0.1),
                                onTap: () {
                                  context.read<GenerBloc>().add(SelectGener(
                                      id: state.gener[index].trim()));
                                  context.read<AnimeByGenreBloc>().add(
                                      GetAnimeByGener(
                                          genre: state.gener[index]));
                                  // context.read<AnimeBloc>().add(GetMovieEvent(
                                  //     genre: state.gener[index].trim()));
                                },
                                child: Text(
                                  state.gener[index],
                                  style: textStyle(
                                      size: media(context).width * 0.07,
                                      color:
                                          state.gener[index] == state.selectedid
                                              ? Colors.grey[600]!
                                              : Colors.grey[400]!,
                                      weight: FontWeight.normal),
                                ),
                              ),
                            ),
                            scrollDirection: Axis.horizontal,
                            physics: AlwaysScrollableScrollPhysics(),
                          ),
                        );
                      }
                      if (state is ErrorStateG) {
                        return const Center();
                      } else {
                        return SizedBox();
                      }
                    },
                  ),
                  BlocBuilder<AnimeByGenreBloc, AnimeByGenreState>(
                    bloc: context.read<AnimeByGenreBloc>()
                      ..add(GetAnimeByGener(genre: "Action")),
                    builder: (context, state) {
                      if (state is AnimeByGenreInitial) {
                        return SizedBox(
                            width: media(context).width,
                            height: media(context).height * 0.4,
                            child: SkeletonGrid(mediaQuery: media(context)));
                      }
                      if (state is AnimeByGenreLoaded) {
                        return SizedBox(
                          width: media(context).width,
                          height: media(context).height * 0.4,
                          child: GridView.builder(
                            shrinkWrap: true,
                            itemCount: state.anime.length,
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                                    childAspectRatio: 6 / 9,
                                    crossAxisCount: 2,
                                    crossAxisSpacing: 5),
                            itemBuilder: (context, index) => InkWell(
                              onTap: () {
                                Navigator.of(context).pushNamed(
                                    AnimeDetiels.routeName,
                                    arguments: state.anime[index]);
                              },
                              child: Column(
                                children: [
                                  Card(
                                      color: Colors.transparent,
                                      child: Container(
                                        clipBehavior: Clip.antiAlias,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(15)),
                                        height: media(context).width * 0.48,
                                        width: media(context).width * 0.38,
                                        child: Image(
                                          errorBuilder:
                                              (context, error, stackTrace) =>
                                                  const Icon(
                                            Icons.error,
                                            color: Colors.red,
                                          ),
                                          fit: BoxFit.fitWidth,
                                          image: NetworkImage(
                                              state.anime[index].image!),
                                        ),
                                      )),
                                  SizedBox(
                                      width: media(context).width * 0.4,
                                      child: Text(
                                        state.anime[index].title!,
                                        maxLines: 2,
                                        style: textStyle(
                                            size: media(context).width * 0.05,
                                            color: Colors.grey[900]!,
                                            weight: FontWeight.normal),
                                      ))
                                ],
                              ),
                            ),
                          ),
                        );
                      } else {
                        return SizedBox();
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
