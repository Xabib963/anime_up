import 'package:ani_me/core/Theme.dart';
import 'package:ani_me/core/settings.dart';

import 'package:ani_me/presentation/loadWidget/SkeletonList.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../Logic/Bloc/SearchBloc/search_bloc.dart';

import '../AnimeDieatels/AnimeDiatels.dart';

class SearchView extends StatelessWidget {
  const SearchView({super.key});
  static const routeName = '/Search';
  @override
  Widget build(BuildContext context) {
    String query = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          'Result OF \"$query \"',
          style: textStyle(
              size: 20, color: Colors.grey[700]!, weight: FontWeight.bold),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_new,
            color: Colors.grey[700]!,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        scrolledUnderElevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: Container(
          height: media(context).height,
          child: BlocBuilder<SearchBloc, SearchState>(
            bloc: context.read<SearchBloc>()..add(GetSearchResult(name: query)),
            builder: (context, state) {
              if (state is SearchInitial) {
                return SkeletonList(
                  mediaQuery: media(context),
                );
              }
              if (state is SearchListLoaded) {
                return ListView.builder(
                  itemCount: state.dataList.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                        onTap: () {
                          Navigator.of(context).pushNamed(
                              AnimeDetiels.routeName,
                              arguments: state.dataList[index]);
                        },
                        leading: CircleAvatar(
                            radius: 30,
                            onBackgroundImageError: (exception, stackTrace) =>
                                Icon(
                                  Icons.error,
                                  color: Colors.red,
                                ),
                            backgroundImage:
                                NetworkImage(state.dataList[index].image!)),
                        title: Text(
                          state.dataList[index].title!,
                          style: textStyle(
                              size: 18,
                              color: Colors.grey[700]!,
                              weight: FontWeight.normal),
                          maxLines: 2,
                        ));
                  },
                );
              } else {
                context.read<SearchBloc>()..add(GetSearchResult(name: 'One'));
                return SizedBox();
              }
            },
          )),
    );
  }
}
