import 'package:ani_me/Logic/Bloc/AnimeByGenre/bloc/anime_by_genre_bloc.dart';
import 'package:ani_me/presentation/Home/MyHomePage.dart';
import 'package:ani_me/presentation/SearchPage/SearchPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../Logic/Bloc/AnimeBloc/anime_bloc.dart';
import '../Logic/Bloc/GenerBloc/gener_bloc.dart';
import '../Logic/Bloc/SearchBloc/search_bloc.dart';
import '../data/datasources/GenerServices.dart';
import '../data/datasources/SearchFor.dart';
import '../data/datasources/getMovieAnimes.dart';
import '../presentation/AnimeDieatels/AnimeDiatels.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    final argument = settings.arguments;
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => MultiRepositoryProvider(
            providers: [
              RepositoryProvider<AnimeServices>(
                create: (context) => AnimeServices(),
              ),
              RepositoryProvider<GenerServices>(
                create: (context) => GenerServices(),
              ),
            ],
            child: MultiBlocProvider(providers: [
              BlocProvider<AnimeBloc>(
                create: (context) =>
                    AnimeBloc(services: context.read<AnimeServices>()),
              ),
              BlocProvider<GenerBloc>(
                create: (context) =>
                    GenerBloc(generServices: context.read<GenerServices>()),
              ),
              BlocProvider<AnimeByGenreBloc>(
                create: (context) => AnimeByGenreBloc(),
              )
            ], child: const MyHomePage()),
          ),
        );
      case '/Search':
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => RepositoryProvider<SearchServices>(
            create: (context) => SearchServices(),
            child: BlocProvider<SearchBloc>(
              create: (context) => SearchBloc(context.read<SearchServices>()),
              child: SearchView(),
            ),
          ),
        );
      case '/detiels':
        return MaterialPageRoute(
          builder: (context) => AnimeDetiels(),
          settings: settings,
        );
      default:
        return MaterialPageRoute(
          settings: settings,
          builder: (_) => MyHomePage(),
        );
    }
  }
}
