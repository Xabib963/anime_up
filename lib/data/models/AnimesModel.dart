class Animes {
  String? sId;
  String? title;
  List<String>? alternativeTitles;
  int? ranking;
  List<String>? genres;
  int? episodes;
  bool? hasEpisode;
  bool? hasRanking;
  String? image;
  String? link;
  String? status;
  String? synopsis;
  String? thumb;
  String? type;

  Animes(
      {this.sId,
      this.title,
      this.alternativeTitles,
      this.ranking,
      this.genres,
      this.episodes,
      this.hasEpisode,
      this.hasRanking,
      this.image,
      this.link,
      this.status,
      this.synopsis,
      this.thumb,
      this.type});

  Animes.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    alternativeTitles = json['alternativeTitles'].cast<String>();
    ranking = json['ranking'];
    genres = json['genres'].cast<String>();
    episodes = json['episodes'];
    hasEpisode = json['hasEpisode'];
    hasRanking = json['hasRanking'];
    image = json['image'];
    link = json['link'];
    status = json['status'];
    synopsis = json['synopsis'];
    thumb = json['thumb'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['alternativeTitles'] = this.alternativeTitles;
    data['ranking'] = this.ranking;
    data['genres'] = this.genres;
    data['episodes'] = this.episodes;
    data['hasEpisode'] = this.hasEpisode;
    data['hasRanking'] = this.hasRanking;
    data['image'] = this.image;
    data['link'] = this.link;
    data['status'] = this.status;
    data['synopsis'] = this.synopsis;
    data['thumb'] = this.thumb;
    data['type'] = this.type;
    return data;
  }
}
