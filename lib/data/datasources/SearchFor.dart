import 'dart:convert';

import '../models/AnimesModel.dart';
import 'package:http/http.dart' as http;

import '../models/SearchSample.dart';
import '../repositories/SearchForRepositry.dart';

class SearchServices extends SearchForRepositry {
  @override
  Future<List<Animes>> searchforAnime(String name) async {
    String url = '';
    try {
      var response = await http.get(Uri.parse(url), headers: {});

      List<Animes> animes = (json.decode(response.body))['data']
          .map<Animes>((ele) => Animes.fromJson(ele))
          .toList();

      return animes;
    } catch (e) {
      throw Exception(e);
    }
  }
}
